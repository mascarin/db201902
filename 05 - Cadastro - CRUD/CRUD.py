import psycopg2

conn = psycopg2.connect(host="200.134.10.32",dbname="201902NVM", user="201902NVM", password="372009")
cur = conn.cursor()
def menuPrincipal(op):
    "Imprime o menu principal e retorna a opção escolhida"
    op = -1
    while(op != '0' and op != '1' and op != '2'):
        print("\n1 - Cadastrar nova pessoa")
        print("2 - Listar pessoas")
        print("0 - Sair")
        op = input("Selecione uma das opções: ")
        if(op != '0' and op != '1' and op != '2'):
            print("\n\nOpcao Invalida!")
        return op

def atualizar():
    uri = "http://utfpr.edu.br/CSB30/2019/2/" + input("Digite o final do uri do usuario que deseja editar (Ex: DI902usuariopadrao): ")
    campo = input("Digite o campo que deseja alterar (Ex: name, uri, hometown): ")
    valor = input("Digite o que deseja colocar no lugar de %s: " % campo)
    if(campo == "uri"):
        valor = "http://utfpr.edu.br/CSB30/2019/2/" + valor
    try:
        cur.execute("UPDATE person SET {0} = '{1}' WHERE uri = '{2}' ".format(campo, valor, uri))
    except psycopg2.Error as error:
        print("Impossivel alterar valor: ", error)
        conn.rollback()
    else:
        conn.commit()
    return

def deletar():
    uri2 = input("Digite o final do uri do usuario que deseja deletar (Ec: DI902usuariopadrao) ou digite 0 para cancelar: ")
    if (uri2=='0'):
        return
    uri = "http://utfpr.edu.br/CSB30/2019/2/" + uri2
    flag  = '0'
    try:
        flag = cur.execute("DELETE FROM person WHERE uri = '{0}'".format(uri))
    except psycopg2.Error as error:
        print("Nao foi possivel deletar: ", error)
        conn.rollback()
    else:
        conn.commit()
    
    return

def adicionarAmigos(uri):
    cur.execute("SELECT uri FROM person")
    pessoa = -1
    while(pessoa != "http://utfpr.edu.br/CSB30/2019/2/0"):
        pessoa = "http://utfpr.edu.br/CSB30/2019/2/" + input("Digite 0 para sair ou o final do uri para adicionar como amigo (Ex:DI902usuariopadrao): ")
        if(pessoa != "http://utfpr.edu.br/CSB30/2019/2/0"):
            try:
                cur.execute("INSERT INTO \"RedeSocialNovo\".adiciona (usuario1, usuario2) VALUES ('{0}', '{1}')".format(uri, pessoa))
            except psycopg2.Error as error:
                print("Impossivel adicionar como amigo: ", error)
                conn.rollback()
            else:
                conn.commit()
    return


def inserir():

    flag = 0
    query = "INSERT INTO person (name, uri, hometown) VALUES (%s, %s, %s)"
    op= input("Digite o uri do Usuario (Ex: DI902usuariopadrao) ou digite 0 para cancelar:")
    if (op=='0'):
        return
    uri = "http://utfpr.edu.br/CSB30/2019/2/" + op
    name = input("Nome Completo do Usuario: ")
    hometown = input("Cidade Natal do Usuario: ")
    dados = (name, uri, hometown)
    try:
        cur.execute(query, dados)
    except psycopg2.IntegrityError as error:
        print("Falha ao inserir usuario: ", error)
        conn.rollback()
    else:
        flag = 1
        conn.commit()
    if(flag == 1):
        flag2 = '0'
        while(flag2 != 'y' and flag2 != 'n'):
            flag2 = input("\nDeseja adicionar amigos? (y/n): ")
            if(flag2 == 'y'):
                query = "SELECT * FROM person"
                cur.execute(query)
                lista = cur.fetchall()
                for l in lista:
                    print(l)
                adicionarAmigos(uri)
            elif(flag2 == 'n'):
                break
            else:
                print("Opção invalida!")
    return

def listar():
    query = "SELECT * FROM person"
    cur.execute(query)
    lista = cur.fetchall()
    for l in lista:
        print(l)
    op2 = -1
    while(op2 != '0' and op2 != '1' and op2 != '2'):
        print("\n1 - Atualizar Usuario")
        print("2 - Deletar Usuario")
        print("0 - Voltar ao Menu Principal")
        op2 = input("Selecione uma das opções: ")
        if(op2 != '0' and op2 != '1' and op2 != '2'):
            print("\n\nOpcao Invalida!")
    if(op2 == '1'):
        atualizar()
    elif(op2 == '2'):
        deletar()
    return

flag = 1
option = 0
while(flag):
    option = menuPrincipal(option)
    if(option == '1'):
        inserir()
    elif(option == '2'):
        listar()
    elif(option == '0'):
        flag = 0


conn.commit()
conn.close()
