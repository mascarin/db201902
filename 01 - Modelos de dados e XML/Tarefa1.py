#!/usr/bin/python
# -*- coding: utf-8 -*-

from xml.dom.minidom import parse
import xml.dom.minidom
import csv

# Open XML document using minidom parser
DOMTree = xml.dom.minidom.parse("marvel_simplificado.xml")

universe = DOMTree.documentElement

# Get all the heroes in the universe
heroes = universe.getElementsByTagName("hero")   
 
# Importing the Good and Bad Heroes to their .csv
good_heroes = ([])
bad_heroes = ([])

for hero in heroes:
    myhero = ([hero.getAttribute('id'),
               hero.getElementsByTagName('name')[0].childNodes[0].data,
               hero.getElementsByTagName('popularity')[0].childNodes[0].data,
               hero.getElementsByTagName('gender')[0].childNodes[0].data,
               hero.getElementsByTagName('height_m')[0].childNodes[0].data,
               hero.getElementsByTagName('weight_kg')[0].childNodes[0].data,
               hero.getElementsByTagName('hometown')[0].childNodes[0].data,
               hero.getElementsByTagName('intelligence')[0].childNodes[0].data,
               hero.getElementsByTagName('strength')[0].childNodes[0].data,
               hero.getElementsByTagName('speed')[0].childNodes[0].data,
               hero.getElementsByTagName('durability')[0].childNodes[0].data,
               hero.getElementsByTagName('energy_Projection')[0].childNodes[0].data,
               hero.getElementsByTagName('fighting_Skills')[0].childNodes[0].data])
    if (hero.getElementsByTagName('alignment')[0].childNodes[0].data == 'Good'):
        good_heroes.append(myhero)
    if (hero.getElementsByTagName('alignment')[0].childNodes[0].data == 'Bad'):
        bad_heroes.append(myhero)

with open('good_heroes.csv', 'w') as myfile:
    wr = csv.writer(myfile)
    wr.writerows(good_heroes)
    
with open('bad_heroes.csv', 'w') as myfile:
    wr = csv.writer(myfile)
    wr.writerows(bad_heroes)

prop = float(len(good_heroes)) / float(len(bad_heroes))

print(f"Numero de herois bons: {len(good_heroes)}")
print(f"Numero de herois ruins: {len(bad_heroes)}")
print(f"Proporção de herois bons/maus: {prop:.2f}")


# Average of the weights
weight = 0.0
counter = 0.0
for hero in heroes:
    weight = weight + float(hero.getElementsByTagName('weight_kg')[0].childNodes[0].data)
    counter = counter + 1
    
print(f"Média de peso dos heróis é: {(weight/counter):.2f}")

# Calculating the Hulk IMC
weight = 0.0
height = 0.0
for hero in heroes:
    if (hero.getElementsByTagName('name')[0].childNodes[0].data == 'Hulk'):
        weight = float(hero.getElementsByTagName('weight_kg')[0].childNodes[0].data)
        height = float(hero.getElementsByTagName('height_m')[0].childNodes[0].data)
        print (f"Indice de massa corporal do hulk é: {weight/height*height:.2f}")
        break