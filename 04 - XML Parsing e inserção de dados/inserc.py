import psycopg2
import requests
import xml.etree.ElementTree as ET


conn = psycopg2.connect(host="200.134.10.32",dbname="201902NVM", user="201902NVM", password="372009")

#BLOCO TABELA 1, USUÁRIOS
print("Preenchendo Usuarios")
r = requests.get('http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/person.xml',stream=True)
tree = ET.parse(r.raw)
root = tree.getroot()
cur = conn.cursor()
#loop de usuários, for para os atributos
for user in root.findall('Person'):
        name = user.get('name')
        home = user.get('hometown')
        uriperson = user.get('uri')
        born = user.get('birthdate')
        try:
                cur.execute("INSERT INTO person (uri, name, hometown, birthdate) \
                             VALUES ('{1}', '{0}', '{2}', '{3}')".format(name, uriperson, home, born));
        except psycopg2.IntegrityError:
                conn.rollback()
        except Exception as e:
                print(e)
                pass
        else:
                conn.commit()

#
#BLOCO TABELA 2, ARTISTAS MUSICAIS
#

print("Preenchendo Artistas Musicais e Notas")

r = requests.get('http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/music.xml',stream=True)
tree = ET.parse(r.raw)
root = tree.getroot()
#loop de insert pra artista
for artista in root.findall('LikesMusic'):
        urimusic = artista.get('bandUri')
        rate  = artista.get('rating')
        pmu = artista.get('person')
        
        try:
                cur.execute("INSERT INTO likemusic (personmusic,  rate, urimusic) \
                                 VALUES ('{0}', '{1}', '{2}')".format(pmu, rate, urimusic));
        except psycopg2.IntegrityError:
                conn.rollback()
        else:
                conn.commit()


#
#BLOCO TABELA 3, FILMES
#
print("Preenchendo Filmes e Notas")
r = requests.get('http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/movie.xml',stream=True)
tree = ET.parse(r.raw)
root = tree.getroot()
#loop de insert pra filmes
for film in root.findall('LikesMovie'):
        urimovie = film.get('movieUri')
        rate = film.get('rating')
        pmo = film.get('person')
        try:
                cur.execute("INSERT INTO likemovie (urimovie) \
                          VALUES ('{0}')".format(urimovie));
        except psycopg2.IntegrityError:
                conn.rollback()
        else:
                conn.commit()
        #inserindo na tabela de avaliação de filmes
        try:
                cur.execute("INSERT INTO likemovie (rate,  personmovie, urimovie) \
                             VALUES ('{0}', '{1}', '{2}')".format(rate, pmo, urimovie));
        except psycopg2.IntegrityError:
                conn.rollback()
        else:
                conn.commit()


#
# BLOCO TABELA 4, ADICIONA
#
print("Preenchendo Amigos")
r = requests.get('http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/knows.xml',stream=True)
tree = ET.parse(r.raw)
root = tree.getroot()
#loop de insert
for person in root.findall('Knows'):
        per1 = person.get('person')
        per2 = person.get('colleague')
        try:
                cur.execute("INSERT INTO know (person1, person2) \
                             VALUES ('{0}','{1}')".format(per1, per2));

        except Exception as e:
                conn.rollback()
        else:
                conn.commit()


conn.close()

